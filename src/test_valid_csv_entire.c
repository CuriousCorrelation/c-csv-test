#include <stdio.h>

#include "c-csv.h"

int test_valid_csv_entire()
{
  const char* file_path = "../../c-csv-test/data/AAPL.csv";

  CSV    apple_historical;
  Status read_status = CCSV_UNDEFINED;

  read_status = enumerate_csv_from_file(file_path, ",", "\"", "\"", 1, &apple_historical);

  if (read_status != CCSV_SUCCESS)
    {
      printf("Error parsing csv file: %s\n", file_path);
      printf("Error code is: %d\n", read_status);
      return -1;
    }
  else
    {
      printf("Parsing successful. Freeing.\n");

      free_csv(&apple_historical);
      return 0;
    }
}

int main() { return test_valid_csv_entire(); }
