#include <stdio.h>

#include "c-csv.h"

int test_partially_missing_csv()
{

  const char* file_path = "../../c-csv-test/data/AAPL_partially_missing.csv";

  CSV    apple_historical;
  Status read_status = CCSV_UNDEFINED;

  read_status = enumerate_csv_from_file(file_path, ",", "\"", "\"", 1, &apple_historical);

  if (read_status == CCSV_SUCCESS)
    {
      printf("Trying to parse invalid csv file: %s\n", file_path);
      return -1;
    }
  else
    {
      return 0;
    }
}

int main() { return test_partially_missing_csv(); }
