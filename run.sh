#!/bin/sh

dir=../build
run_command() {
    cd $dir && cmake -DC_CSV_TEST=TRUE .. && cmake -DC_CSV_TEST=TRUE --build . && make && make test
}

if [ ! -e $dir ]
then
    mkdir $dir && run_command
else
    run_command
fi
