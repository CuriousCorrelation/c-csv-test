#include <stdio.h>

#include "c-csv.h"

int test_valid_csv_entire(const char* file_path)
{
  CSV    csv_data;
  Status read_status = CCSV_UNDEFINED;

  read_status = enumerate_csv_from_file(file_path, ",", "\"", "\"", 1, &csv_data);

  if (read_status != CCSV_SUCCESS)
    {
      printf("Error parsing csv file: %s\n", file_path);
      printf("Error code is: %d\n", read_status);
      return -1;
    }
  else
    {
      printf("Parsing successful. Freeing.\n");

      puts("Header\n");

      for (int i = 0; i < csv_data.number_of_columns; ++i)
        {
          printf("%s ", csv_data.header[i]);
        }

      for (int i = 0; i < 10; ++i)
        {
          for (int j = 0; j < csv_data.number_of_columns; ++j)
            {
              printf("%s ", csv_data.table[i][j]);
            }
        }

      free_csv(&csv_data);
      return 0;
    }
}

int main(int argc, char* argv[]) { return test_valid_csv_entire(argv[1]); }
